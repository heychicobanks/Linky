import { StatusBar } from 'expo-status-bar';
import { StyleSheet, Text, View } from 'react-native';
import { Footer } from './src/components/Footer/Footer';
import { Navigation } from './src/navigation/Navigation';

export default function App() {
  return (
      <Navigation />
  );
}
