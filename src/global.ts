import { Dimensions } from "react-native";
import TestScreen from "./screens/OffersScreen";
import OffersScreen from "./screens/OffersScreen";

export const {width, height} = Dimensions.get('window');
export const MARGIN = 16
export const TAB_BAR_WIDTH = width - 2 * MARGIN

export const screenArray = [
    {
        route: "Offres",
        activeIcon: "file-text",
        inactiveIcon: "file-text",
        hasFilter: false,
        tabBarColor: "tomato",
        component: OffersScreen,
      }, 
    {
        route: "Favoris",
        activeIcon: "folder",
        inactiveIcon: "folder",
        hasFilter: false,
        tabBarColor: "DeepSkyBlue",
        component: TestScreen,
    },
    {
        route: "Profil",
        activeIcon: "user",
        inactiveIcon: "user",
        hasFilter: false,
        tabBarColor: "DeepSkyBlue",
        component: TestScreen,
    },
    {
        route: "Messages",
        activeIcon: "message-square",
        inactiveIcon: "message-square",
        hasFilter: false,
        tabBarColor: "DeepSkyBlue",
        component: TestScreen,
      },
      
];
  