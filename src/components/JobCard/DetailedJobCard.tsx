import { BlurView } from '@react-native-community/blur';
import { LinearGradient } from 'expo-linear-gradient';
import React, { useRef, useState } from 'react';
import { View, Text, StyleSheet, Image, TouchableOpacity, ScrollView, Dimensions, SafeAreaView, Platform, StatusBar, Animated } from 'react-native';
import { FlatList } from 'react-native-gesture-handler';
import Icon from 'react-native-vector-icons/MaterialIcons';
import { JobLocationMap } from '../JobLocationMap/JobLocationMap';

const { width, height } = Dimensions.get('window');

type JobOffer = {
  id: number;
  title: string;
  salary: string;
  contractType: string;
  companyName: string;
  location: string;
  logo: string;
  description: string;
  keywords: string[];
  createdAt: string;
  experienceLevel: string;
};

type DetailedJobCardProps = {
  job: JobOffer;
  onClose: () => void;
};

const DetailedJobCard: React.FC<DetailedJobCardProps> = ({ job, onClose }) => {
  const [currentPage, setCurrentPage] = useState(0);
  const scrollX = useRef(new Animated.Value(0)).current;
  const flatListRef = useRef<FlatList>(null);

  const renderKeywords = (keywords: string[]) => {
    return (
      <View style={styles.keywordsContainer}>
        {keywords.map((keyword, index) => (
          <View key={index} style={styles.keyword}>
            <Text style={styles.keywordText}>{keyword}</Text>
          </View>
        ))}
      </View>
    );
  };

  const renderDots = () => {
    return (
      <View style={styles.dotsContainer}>
        {[0, 1, 2].map((_, index) => (
          <TouchableOpacity 
            key={index} 
            style={[styles.dot, currentPage === index && styles.activeDot]}
            onPress={() => {
              flatListRef.current?.scrollToIndex({ index, animated: true });
              setCurrentPage(index);
            }}
          />
        ))}
      </View>
    );
  };

  const renderItem = ({ item, index }: { item: any; index: number }) => {
    switch (index) {
      case 0:
        return (
          <View style={styles.page}>
            <View style={styles.infoContainer}>
              <View style={styles.infoItem}>
                <Icon name="attach-money" size={24} color="#4caf50" />
                <Text style={styles.infoLabel}>Salaire</Text>
                <Text style={styles.infoValue}>{job.salary}</Text>
              </View>
              <View style={styles.infoItem}>
                <Icon name="business-center" size={24} color="#ff9800" />
                <Text style={styles.infoLabel}>Type de contrat</Text>
                <Text style={styles.infoValue}>{job.contractType}</Text>
              </View>
              <View style={styles.infoItem}>
                <Icon name="location-on" size={24} color="#e91e63" />
                <Text style={styles.infoLabel}>Localisation</Text>
                <Text style={styles.infoValue}>{job.location}</Text>
              </View>
              <View style={styles.infoItem}>
                <Icon name="school" size={24} color="#9c27b0" />
                <Text style={styles.infoLabel}>Niveau</Text>
                <Text style={styles.infoValue}>{job.experienceLevel}</Text>
              </View>
            </View>
            {renderKeywords(job.keywords)}
          </View>
        );
      case 1:
        return (
          <View style={styles.page}>
            <Text style={styles.sectionTitle}>Description du poste</Text>
            <Text style={styles.description}>{job.description}</Text>
            <Text style={styles.dateInfo}>Publié le : {job.createdAt}</Text>
          </View>
        );
      case 2:
        return (
          <View style={styles.page}>
            <JobLocationMap location={job.location} companyName={job.companyName} />
          </View>
        );
      default:
        return null;
    }
  };

  return (
    <SafeAreaView style={styles.safeArea}>
      <StatusBar barStyle="light-content" />
      <View style={styles.header}>
        <TouchableOpacity style={styles.closeButton} onPress={onClose}>
          <Icon name="arrow-back" size={24} color="#fff" />
        </TouchableOpacity>
        <View style={styles.headerContent}>
          <Text style={styles.title}>{job.title}</Text>
          <Text style={styles.companyName}>{job.companyName}</Text>
        </View>
      </View>

      <Animated.FlatList
        ref={flatListRef}
        data={[0, 1, 2]}
        renderItem={renderItem}
        keyExtractor={(item) => item.toString()}
        horizontal
        pagingEnabled
        showsHorizontalScrollIndicator={false}
        onScroll={Animated.event(
          [{ nativeEvent: { contentOffset: { x: scrollX } } }],
          { useNativeDriver: true }
        )}
        onMomentumScrollEnd={(event) => {
          const newIndex = Math.round(event.nativeEvent.contentOffset.x / width);
          setCurrentPage(newIndex);
        }}
        scrollEventThrottle={16}
      />

      {renderDots()}

      <TouchableOpacity style={styles.applyButton} onPress={() => console.log('Apply pressed')}>
      </TouchableOpacity>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  safeArea: {
    flex: 1,
    backgroundColor: '#f5f5f5',
  },
  header: {
    height: height * 0.3,
    justifyContent: 'flex-end',
    padding: 20,
    overflow: 'hidden',
  },
  backgroundImage: {
    ...StyleSheet.absoluteFillObject,
    width: '100%',
    height: '100%',
  },
  headerContent: {
    alignItems: 'center',
  },
  closeButton: {
    position: 'absolute',
    top: 20,
    left: 20,
    zIndex: 1,
  },
  logo: {
    width: 80,
    height: 80,
    borderRadius: 40,
    marginBottom: 10,
    borderWidth: 2,
    borderColor: '#fff',
  },
  title: {
    fontSize: 24,
    fontWeight: 'bold',
    color: '#fff',
    textAlign: 'center',
    textShadowColor: 'rgba(0, 0, 0, 0.75)',
    textShadowOffset: { width: -1, height: 1 },
    textShadowRadius: 10,
  },
  companyName: {
    fontSize: 16,
    color: '#e0e0e0',
    marginTop: 5,
    textShadowColor: 'rgba(0, 0, 0, 0.75)',
    textShadowOffset: { width: -1, height: 1 },
    textShadowRadius: 10,
  },
  page: {
    width: width - 30,
    padding: 20,
    borderRadius: 20,
    backgroundColor: '#fff',
  },
  infoContainer: {
    flexDirection: 'row',
    flexWrap: 'wrap',
    justifyContent: 'space-between',
    marginBottom: 20,
  },
  infoItem: {
    width: '48%',
    alignItems: 'center',
    marginBottom: 10,
    padding: 6,
  },
  infoLabel: {
    fontSize: 12,
    color: '#666',
    marginTop: 5,
  },
  infoValue: {
    fontSize: 14,
    fontWeight: '600',
    color: '#333',
    marginTop: 2,
    textAlign: 'center',
  },
  sectionTitle: {
    fontSize: 22,
    fontWeight: '600',
    color: '#333',
    marginBottom: 15,
  },
  description: {
    fontSize: 16,
    color: '#555',
    lineHeight: 24,
  },
  keywordsContainer: {
    flexDirection: 'row',
    flexWrap: 'wrap',
  },
  keyword: {
    backgroundColor: '#f0f0f0',
    borderRadius: 20,
    paddingVertical: 8,
    paddingHorizontal: 14,
    marginRight: 8,
    marginBottom: 8,
  },
  keywordText: {
    fontSize: 14,
    color: '#333',
  },
  dateInfo: {
    fontSize: 14,
    color: '#888',
    fontStyle: 'italic',
    marginTop: 10,
  },
  dotsContainer: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    height: 50,
  },
  dot: {
    width: 8,
    height: 8,
    borderRadius: 4,
    backgroundColor: '#bbb',
    marginHorizontal: 5,
  },
  activeDot: {
    backgroundColor: '#3b5998',
    width: 12,
    height: 12,
    borderRadius: 6,
  },
  applyButton: {
    backgroundColor: '#4caf50',
    paddingVertical: 15,
    alignItems: 'center',
    marginHorizontal: 20,
    borderRadius: 10,
    marginBottom: 20,
    elevation: 4,
    shadowColor: '#000',
    shadowOffset: { width: 0, height: 2 },
    shadowOpacity: 0.2,
    shadowRadius: 4,
  },
  applyButtonText: {
    color: '#fff',
    fontSize: 18,
    fontWeight: '600',
  },
});

export default DetailedJobCard;