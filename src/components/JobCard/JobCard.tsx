import React, { useEffect } from 'react';
import { View, Text, StyleSheet, Image, TouchableOpacity, Dimensions } from 'react-native';
import Animated, { useAnimatedGestureHandler, useAnimatedStyle, useSharedValue, withSpring } from 'react-native-reanimated';
import Icon from 'react-native-vector-icons/MaterialIcons';

const { width, height } = Dimensions.get('window');

type JobOffer = {
  id: number;
  title: string;
  salary: string;
  contractType: string;
  location: string;
  logo: string;
  description: string;
  keywords: string[];
  distance: string;
};

type UserProfile = {
  id: number;
  name: string;
  keywords: string[];
};

type JobCardProps = {
  job: JobOffer;
  userProfile: UserProfile;
  onPress: () => void;
};

const SWIPE_THRESHOLD = 100;

const calculateCompatibility = (job: JobOffer, userProfile: UserProfile): number => {
  const matchingKeywords = job.keywords.filter(keyword => userProfile.keywords.includes(keyword));
  const compatibilityScore = (matchingKeywords.length / job.keywords.length) * 100;
  return Math.round(compatibilityScore);
};

const JobCard = ({ job, userProfile, onPress, onSwipeLeft, onSwipeUp, onSwipeRight }) => {
  const compatibility = calculateCompatibility(job, userProfile);

  return (
    <TouchableOpacity style={styles.card} onPress={onPress}>
      <Image source={{ uri: job.logo }} style={styles.logo} />
      <View style={styles.compatibilityContainer}>
          <Icon name="star" size={18} color="#FFD700" />
          <Text style={styles.compatibilityText}>{compatibility}%</Text>
        </View>
      <View style={styles.infoContainer}>
        <Text style={styles.title}>{job.title}</Text>
        <Text style={styles.companyName}>{job.companyName}</Text>
        <View style={styles.detailsContainer}>
          <View style={styles.detailItem}>
            <Icon name="euro-symbol" size={18} color="#757575" />
            <Text style={styles.detailText}>{job.salary}</Text>
          </View>
          <View style={styles.detailItem}>
            <Icon name="location-on" size={18} color="#757575" />
            <Text style={styles.detailText}>{job.location}</Text>
          </View>
        </View>
        <View style={styles.keywordsContainer}>
          {job.keywords.slice(0, 3).map((keyword, index) => (
            <View key={index} style={styles.keyword}>
              <Text style={styles.keywordText}>{keyword}</Text>
            </View>
          ))}
        </View>
        <View style={styles.buttonsContainer}>
        <TouchableOpacity style={[styles.button, styles.passButton]} onPress={onSwipeLeft}>
          <Icon name="close" size={30} color="#FF6B6B" />
        </TouchableOpacity>
        <TouchableOpacity style={[styles.button, styles.superLikeButton]} onPress={onSwipeUp}>
          <Icon name="star" size={30} color="#4ECDC4" />
        </TouchableOpacity>
        <TouchableOpacity style={[styles.button, styles.likeButton]} onPress={onSwipeRight}>
          <Icon name="favorite" size={30} color="#45B649" />
        </TouchableOpacity>
        </View>
      </View>
    </TouchableOpacity>
  );
};

const styles = StyleSheet.create({
  card: {
    borderRadius: 10,
    backgroundColor: 'white',
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 10,
    },
    shadowOpacity: 10,
    shadowRadius: 10,
  },
  logo: {
    width: '100%',
    height: '60%',
    borderTopLeftRadius: 10,
    borderTopRightRadius: 10,
  },
  compatibilityContainer: {
    position: 'absolute',
    top: 10,
    right: 10,
    flexDirection: 'row',
    alignItems: 'center',
    backgroundColor: 'rgba(255, 255, 255, 0.8)',
    borderRadius: 15,
    padding: 5,
  },
  compatibilityText: {
    fontSize: 16,
    fontWeight: 'bold',
    color: '#333',
    marginLeft: 5,
  },
  infoContainer: {
    padding: 20,
  },
  title: {
    fontSize: 24,
    fontWeight: 'bold',
    color: '#333',
  },
  companyName: {
    fontSize: 18,
    color: '#666',
    marginBottom: 10,
  },
  detailsContainer: {
    flexDirection: 'row',
    marginBottom: 10,
  },
  detailItem: {
    flexDirection: 'row',
    alignItems: 'center',
    marginRight: 20,
  },
  detailText: {
    fontSize: 16,
    color: '#757575',
    marginLeft: 5,
  },
  keywordsContainer: {
    flexDirection: 'row',
  },
  keyword: {
    backgroundColor: '#f0f0f0',
    borderRadius: 20,
    paddingVertical: 5,
    paddingHorizontal: 10,
    marginRight: 10,
  },
  keywordText: {
    fontSize: 14,
    color: '#666',
  },
  buttonsContainer: {
    flexDirection: 'row',
    justifyContent: 'space-around',
    marginTop: 20,
  },
  button: {
    width: 60,
    height: 60,
    borderRadius: 30,
    justifyContent: 'center',
    alignItems: 'center',
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5,
  },
  passButton: {
    backgroundColor: '#FFF0F0',
  },
  superLikeButton: {
    backgroundColor: '#F0FFFF',
  },
  likeButton: {
    backgroundColor: '#F0FFF0',
  }
});

export default JobCard;