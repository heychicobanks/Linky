import { View, StyleSheet } from "react-native";
import MapView, { Marker } from "react-native-maps";
import { cityCoordinates } from "../../types/CityCoordinates";

export const JobLocationMap = ({ location, companyName }) => {
  const coordinates = cityCoordinates[location] || { latitude: 48.8566, longitude: 2.3522 }; // Paris par défaut

  const region = {
    ...coordinates,
    latitudeDelta: 0.02,
    longitudeDelta: 0.02,
  };

  return (
    <View style={styles.mapContainer}>
      <MapView
        style={styles.map}
        initialRegion={region}
      >
        <Marker
          coordinate={coordinates}
          title={companyName}
          description={location}
        />
      </MapView>
    </View>
  );
};

const styles = StyleSheet.create({
  mapContainer: {
    flex: 1,
    overflow: "hidden",
    borderRadius: 20,
  },
  map: {
    ...StyleSheet.absoluteFillObject,
  },
});