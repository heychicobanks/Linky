import React from 'react';
import { View, Text, StyleSheet, TouchableOpacity } from 'react-native';

type SwipeButtonsProps = {
  onPass: () => void;
  onApply: () => void;
  onSave: () => void;
};

const SwipeButtons: React.FC<SwipeButtonsProps> = ({ onPass, onApply, onSave }) => {
  return (
    <View style={styles.buttonsContainer}>
      <TouchableOpacity style={styles.button} onPress={onPass}>
        <Text style={styles.buttonText}>Passer</Text>
      </TouchableOpacity>
      <TouchableOpacity style={styles.button} onPress={onApply}>
        <Text style={styles.buttonText}>Postuler</Text>
      </TouchableOpacity>
      <TouchableOpacity style={styles.button} onPress={onSave}>
        <Text style={styles.buttonText}>Enregistrer</Text>
      </TouchableOpacity>
    </View>
  );
};

const styles = StyleSheet.create({
  buttonsContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    width: '90%',
    position: 'absolute',
    bottom: 50,
  },
  button: {
    flex: 1,
    alignItems: 'center',
    paddingVertical: 10,
    marginHorizontal: 5,
    borderRadius: 5,
    backgroundColor: '#007bff',
  },
  buttonText: {
    color: '#fff',
    fontSize: 16,
  },
});

export default SwipeButtons;
