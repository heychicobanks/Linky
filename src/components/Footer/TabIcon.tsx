import { StyleSheet, Text, View, Animated } from "react-native";
import React, { useState, useEffect } from "react";
import Icon from "react-native-vector-icons/Feather";

interface TabIconProps {
  index: number;
  isFocused: boolean;
  tabIcon: { activeIcon: string; inactiveIcon: string };
  label: string;
}

const TabIcon: React.FC<TabIconProps> = ({
  index,
  isFocused,
  tabIcon,
  label,
}) => {
  const [translateY] = useState(new Animated.Value(0));

  const translateIcon = (val: number) => {
    Animated.spring(translateY, {
      toValue: val,
      useNativeDriver: true,
    }).start();
  };

  useEffect(() => {
    if (isFocused) {
      translateIcon(-15);
    } else {
      translateIcon(0);
    }
  }, [index]);
  console.log;
  return (
    <>
      <Animated.View
        style={{
          transform: [{ translateY }],
          alignItems: "center",
          justifyContent: "center",
          opacity: isFocused ? 1 : 0.5,
        }}
      >
        <Icon
          name={isFocused ? tabIcon.activeIcon : tabIcon.inactiveIcon}
          size={24}
          color={isFocused ? "white" : "gray"}
        />
      </Animated.View>
      <Text style={{ color: isFocused ? "tomato" : "gray" }}>{label}</Text>
    </>
  );
};

export default TabIcon;