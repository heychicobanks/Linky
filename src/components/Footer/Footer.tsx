import {Animated, View, TouchableOpacity, StyleSheet} from 'react-native';
import { useState, useEffect} from 'react'
import { MARGIN, TAB_BAR_WIDTH, screenArray } from '../../global';
import TabIcon from './TabIcon';

interface TabBarProps {
  state: any,
  descriptors: any,
  navigation: any
}

const TAB_WIDTH = TAB_BAR_WIDTH / screenArray.length

export const Footer: React.FC<TabBarProps> = ({ state, descriptors, navigation }) => {
    const [translateX] = useState(new Animated.Value(0));

    
    const translateTab = (index: number) => {
        Animated.spring(translateX, {
            toValue: index * TAB_WIDTH,
            useNativeDriver: true,
        }).start()
    }

    useEffect(() => {
        translateTab(state.index)
    }, [state.index]);

  return (
    <View style={styles.tabBarContainer}>
        <View style = {styles.slidingTabContainer}>
            <Animated.View style = {[styles.slidingTab, {transform: [{translateX}]}]}/>
        </View>
      {state.routes.map((route: any, index: number) => {
        const { options } = descriptors[route.key];
        const label =
          options.tabBarLabel !== undefined
            ? options.tabBarLabel
            : options.title !== undefined
            ? options.title
            : route.name;

        const isFocused = state.index === index;

        const onPress = () => {
          const event = navigation.emit({
            type: 'tabPress',
            target: route.key,
            canPreventDefault: true,
          });

          if (!isFocused && !event.defaultPrevented) {
            navigation.navigate(route.name, route.params);
          }
        };

        const onLongPress = () => {
          navigation.emit({
            type: 'tabLongPress',
            target: route.key,
          });
        };

        return (
          <TouchableOpacity
            key={index}
            onPress={onPress}
            onLongPress={onLongPress}
            style={{flex: 1, alignItems: 'center'}}
          >
            <TabIcon label={label} tabIcon={options.tabBarIcon}  isFocused={isFocused} index={state.index}/>
          </TouchableOpacity>
        );
      })}
    </View>
  );
}


const styles = StyleSheet.create({
    tabBarContainer: {
        flexDirection: 'row',
        width: TAB_BAR_WIDTH,
        height: 60,
        position: 'absolute',
        alignSelf: 'center',
        bottom: MARGIN,
        backgroundColor: 'white',
        borderRadius: 10,
        alignItems: 'center',
        justifyContent: 'space-around',
    },
    slidingTabContainer: {
        width: TAB_WIDTH,
        ...StyleSheet.absoluteFillObject,
        borderRadius: 30,
        backgroundColor: 'white',
        alignItems: 'center'
    },
    slidingTab: {
        width: 60,
        height: 60,
        borderRadius: 99999,
        backgroundColor: 'tomato',
        bottom: 25,
        borderWidth: 5,
        borderColor: 'white'
    }
})