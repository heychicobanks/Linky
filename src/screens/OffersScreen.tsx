import React, { useState, useRef } from 'react';
import { View, StyleSheet, Dimensions, SafeAreaView, Text } from 'react-native';
import Swiper from 'react-native-deck-swiper';
import JobCard from '../components/JobCard/JobCard';
import DetailedJobCard from '../components/JobCard/DetailedJobCard';
import SwipeButtons from '../components/SwipeButtons/SwipeButtons';
import { cards } from '../data/jobOffers';

const { width, height } = Dimensions.get('window');

type JobOffer = {
  id: number;
  title: string;
  salary: string;
  distance: string;
  contractType: string;
  companyName: string;
  location: string;
  logo: string;
  description: string;
  keywords: string[];
  createdAt: string;
  experienceLevel: string;
};



type UserProfile = {
  id: number;
  name: string;
  keywords: string[];
};

const userProfile: UserProfile = {
  id: 1,
  name: 'John Doe',
  keywords: ['React', 'Mobile', 'UI/UX', 'JavaScript'],
};

const OffersScreen = () => {
  const [selectedCard, setSelectedCard] = useState<JobOffer | null>(null);
  const swiperRef = useRef<any>(null);

  const onSwipedLeft = (cardIndex: number) => {
    console.log('Passed:', cards[cardIndex]);
    () => swiperRef.current.swipeLeft()
  };

  const onSwipedRight = (cardIndex: number) => {
    console.log('Favorited:', cards[cardIndex]);
  };

  const onSwipedTop = (cardIndex: number) => {
    console.log('Applied:', cards[cardIndex]);
  };

  const renderCard = (card: JobOffer, index: number) => (
    <JobCard job={card} userProfile={userProfile}
    onSwipeLeft={() => onSwipedLeft(index)}
    onSwipeUp={() => onSwipedRight(index)}
    onSwipeRight={() => onSwipedTop(index)}
    onPress={() => setSelectedCard(card)} />
  );

  return (
    <SafeAreaView style={styles.safeArea}>
    <View>
      {selectedCard ? (
        <DetailedJobCard job={selectedCard} onClose={() => setSelectedCard(null)} />
      ) : (
        <>
          <Swiper
            ref={swiperRef}
            cards={cards}
            renderCard={renderCard}
            onSwipedLeft={onSwipedLeft}
            onSwipedRight={onSwipedRight}
            onSwipedTop={onSwipedTop}
            cardIndex={0}
            backgroundColor={'#f2f2f2'}
            stackSize={3}
            verticalSwipe={true}
            horizontalSwipe={true}
            disableBottomSwipe={true}
            verticalThreshold={height / 4}
            horizontalThreshold={width / 2}
            stackSeparation={4}
            stackScale={0.2}
            secondCardZoom={5}
            animateCardOpacity={false}
            animateOverlayLabelsOpacity={false}
            stackAnimationFriction={10}
          />
        </>
      )}
    </View>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  safeArea: {
    flex: 1,
    backgroundColor: '#f2f2f2',
  },
  buttonsContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    width: '90%',
    position: 'absolute',
    bottom: 100, // Adjust this value to position the buttons above the footer
  },
  button: {
    flex: 1,
    alignItems: 'center',
    paddingVertical: 10,
    marginHorizontal: 5,
    borderRadius: 5,
    backgroundColor: '#007bff',
  },
  buttonText: {
    color: '#fff',
    fontSize: 16,
  },
});

export default OffersScreen;
