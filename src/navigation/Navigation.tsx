import React from "react";
import { NavigationContainer } from "@react-navigation/native";
import { createStackNavigator } from "@react-navigation/stack";
import BottomTab from "./navigator/BottomTab";

const StackNavigator = createStackNavigator();

export const Navigation = () => {
    return (
        <NavigationContainer>
            <BottomTab />
        </NavigationContainer>
    );
}