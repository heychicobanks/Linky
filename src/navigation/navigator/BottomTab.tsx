import React from 'react'
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { Footer } from '../../components/Footer/Footer';
import { screenArray } from '../../global';

const BottomTabNavigator = createBottomTabNavigator();

export const BottomTab = () => {
  return (
    <BottomTabNavigator.Navigator
    initialRouteName={screenArray[0].route}
    screenOptions={({ route }) => ({
      headerShown: false,
    })}
    tabBar={(props) => <Footer {...props} />}
  >
    {screenArray.map((screen, index) => {
      return (
        <BottomTabNavigator.Screen
          key={index}
          name={screen.route}
          component={screen.component}
          options={{
            tabBarIcon: {
              activeIcon: screen.activeIcon,
              inactiveIcon: screen.inactiveIcon,
            },
          }}
        />
      );
    })}
  </BottomTabNavigator.Navigator>
)
}

export default BottomTab;
