export const cityCoordinates = {
    'Paris, France': { latitude: 48.8566, longitude: 2.3522 },
    'Lyon, France': { latitude: 45.7640, longitude: 4.8357 },
    'Marseille, France': { latitude: 43.2965, longitude: 5.3698 },
    'Bordeaux, France': { latitude: 44.8378, longitude: -0.5792 },
    'Lille, France': { latitude: 50.6292, longitude: 3.0573 },
    'Toulouse, France': { latitude: 43.6047, longitude: 1.4442 },
    'Nice, France': { latitude: 43.7102, longitude: 7.2620 },
  };