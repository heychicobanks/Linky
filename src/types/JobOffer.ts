export type JobOffer = {
    id: number;
    title: string;
    salary: string;
    distance: string;
    contractType: string;
    companyName: string;
    location: string;
    logo: string;
    description: string;
    keywords: string[];
    createdAt: string;
    experienceLevel: string;
  };
  